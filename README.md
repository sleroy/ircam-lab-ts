> - [Ircam LAB](http://www.ircamlab.com)
> - Distributed by [Plugivery](https://www.plugivery.com/products/p1680-TS/)
> - IrcamLab’s [Official Dealers](http://www.plugivery.com/about/dealers)
> - Only available by downloading 
> - [Technical Support](http://help.dontcrack.com)

![Logo Ircam Lab](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Lab%20TS/logi_ircam_lab.png)

IRCAM Lab TS stands for "Transpose/Stretching" and is a powerful stand alone software application that revolutionizes the way audio signals can be processed.

Unlike other time stretching applications, "TS" uses an advanced phase vocoder engine ([SuperVP](https://forum.ircam.fr/projects/detail/supervp-for-max/)) as well as the analysis/synthesis functionalities of "[AudioSculpt](https://forum.ircam.fr/projects/detail/antescofo/)", providing high-quality sound processing in real-time. This technology offers amazing results even when used to transpose or stretch audio waveforms radically. It is able to transpose and/or pitch a WAV or AIFF audio file with extremely accurate results and minimal artifacts making it possible to process professional recordings with transparent results. A life saver for audio engineers and broadcast technicians!

[![Ircam Lab TS Guides Tour](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Lab%20TS/ircam_lab_t.s._guided_tour.png)](https://www.youtube.com/embed/sODA3ASUk60?feature=oembed) 

But "TS" is much more than a simple time stretching tool. the application can transform an audio signal to extreme settings transforming it into a totally new sound! Simply drag a WAV or AIFF file into "TS" and stretch it down at 300%. That will give you a quick idea of how powerful it is. Now try the formant, transient, sinus or noise sliders to sculpt your sound and you create totally original audio textures. Sound designers, DJ’s and musicians will be amazed by the unlimited possibilities offered by “TS” to create totally new sounds. From a basic drum loop to pianos, voices or any other audio signal, “TS” can make something totally wild and original out of it…

You will be able to adapt the length of your compositions with ease to fit the needs of your project. If your song has to end more quickly or be extended for any reason, it is a click of a button away. and you will hear it immediately. Film and advertising work with their demands for different lengths of material will be instantaneously available to deliver the new time format. 

Your singer can't reach the lowest note or highest note in your piece, transpose it up and down in real time and let them try it right away.

All this in a simple user-friendly interface with a sound quality and musicality that is astonishing.

## Features ##

- Sound source Waveform visualisation
- Spectral information shown on screen
- Transposition in percentage and scale format
- Powerful Time Stretching
- A unique Shape Preservation mode for voice transformation
- Transient shaping
- Harmonic and Inharmonic signals Remix options
- Midi remote control
- Real time recording directly into the application
- Offline bounce (WAV|AIFF, 16|24|32 bit)

## System Requirements ##

- Mac Version: Intel processors only Minimum 2Ghz, GB RAM, Mac OS 10.6 (Snow Leopard) or higher
- Windows Versio: Intel processors only, 2 GB RAM, Windows XP and higher (32 bit) or Windows 7 (64 bit and 32bit)

> - [Demo Download]( http://download.plugivery.com/pvdl/?do=browse&dir=release&os=macosx&bid=102)
The demo is a fully functional, 15 day trial, version without any limitations. You may download directly from the bottom of the product page [here]( http://download.plugivery.com/pvdl/?do=browse&dir=release&os=macosx&bid=102) and install the full IRCAM Lab "TS" software. Be Careful, it’s addictive!